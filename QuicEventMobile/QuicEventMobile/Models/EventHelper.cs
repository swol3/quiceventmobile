﻿using Plugin.Settings;

namespace QuicEventMobile.Models
{
    public static class EventHelper
    {
        private const string SavedEventsKey = "Saved_Events_List";

        public static string GetEventListAsString() =>  CrossSettings.Current.GetValueOrDefault(SavedEventsKey, string.Empty);
        public static string[] GetEventListAsArray() => GetEventListAsString().Split('|');
        public static string[] GetTestEventListAsArray() => "Test|Test|Test|Test|Test|Test|Test|".Split('|');

        public static void SaveEventList(string eventIdListAsString)
        {
            CrossSettings.Current.AddOrUpdateValue(SavedEventsKey, eventIdListAsString);
        }
    }
}
