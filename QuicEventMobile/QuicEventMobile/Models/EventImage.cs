﻿using Xamarin.Forms;

namespace QuicEventMobile.Models
{
    public class EventImage
    {
        public int Id { get; set; }
        public string EventID { get; set; }
        public string ImageAsBase64 { get; set; }
        
        public ImageSource ImageSource { get; set; }
    }
}
