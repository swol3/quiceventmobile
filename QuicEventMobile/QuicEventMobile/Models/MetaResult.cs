﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuicEventMobile.Models
{
    public class MetaResult
    {
        public string Message { get; set; }
        public List<Exception> Errors { get; set; }
    }
}
