﻿using System;
using System.Collections.Generic;

namespace QuicEventMobile.Models
{
    public class Event
    {
        public string Id { get; set; }
        public int? UserID { get; set; }
        public string Name { get; set; }
        public bool? ContainsScheduleITems { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartDateFormatted => StartDate?.ToString("d");
        public string StartTimeFormatted => StartDate?.ToString("hh:mm tt");
        public string DateMonth => StartDate?.ToString("MMM");
        public string DateDay => StartDate?.ToString("dd");
        public DateTime? EndDate { get; set; }
        public string Location { get; set; }
        public ICollection<EventItem> EventItems { get; set; }
        public ICollection<EventImage> EventImages { get; set; }
    }
}
