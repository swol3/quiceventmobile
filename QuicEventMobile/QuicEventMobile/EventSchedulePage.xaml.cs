﻿using Android.Widget;
using QuicEventMobile.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Application = Android.App.Application;

namespace QuicEventMobile
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventSchedulePage
    {
        public Event MyEvent { get; set; }
        public ObservableCollection<EventImage> ObservableCollection { get; set; }

        public EventSchedulePage(Event myEvent)
        {
            try
            {
                MyEvent = myEvent;
                NavigationPage.SetHasNavigationBar(this, false);
                InitializeComponent();
                ObservableCollection = new ObservableCollection<EventImage>();
                CarouselEventImages.ItemsSource = ObservableCollection;

                foreach (var eventImage in MyEvent.EventImages)
                {
                    var sanitisedImageAsBase64String =
                        eventImage.ImageAsBase64.Substring(eventImage.ImageAsBase64.IndexOf(',') + 1);
                    var sanitisedImageAsByteArray = Convert.FromBase64String(sanitisedImageAsBase64String);
                    eventImage.ImageSource = ImageSource.FromStream(() => new MemoryStream(sanitisedImageAsByteArray));

                    ObservableCollection.Add(eventImage);
                }
            }
            catch (Exception ex)
            {
                Toast.MakeText(Application.Context, ex.Message, ToastLength.Long).Show();
            }
        }

        //todo: Find a way to implelment this on the image https://www.c-sharpcorner.com/article/zoom-in/
        //Also, feel free to implement it on the map viewer as well
    }
}