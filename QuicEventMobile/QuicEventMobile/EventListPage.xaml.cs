﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using QuicEventMobile.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QuicEventMobile
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventListPage 
    {
        public ObservableCollection<Event> ObservableCollection { get; set; }

        private List<Event> Events { get; }

        public EventListPage()
        {
            Events = new List<Event>();
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            ObservableCollection = new ObservableCollection<Event>();
            SavedEventsListView.ItemsSource = ObservableCollection;
            UpdateEventsListAsync();
        }

        private async void UpdateEventsListAsync()
        {
            var failedAttempts = 0;
            var removedEvents = 0;
            var url = "http://test-api.quicevent.com/api/event";
#if !DEBUG
            url = "http://api.quicevent.com/api/event";
#endif

            using (var client = new HttpClient())
            {
                foreach (var myEvent in EventHelper.GetEventListAsArray())
                {
                    var response = await client.GetAsync($"{url}/{myEvent}");
                    if (response.IsSuccessStatusCode)
                    {
                        var apiResult = await response.Content.ReadAsAsync<ApiResult<Event>>();
                        if (null == apiResult.Meta)
                        {
                            var thisEvent = apiResult.Data;
                            Events.Add(thisEvent);

                            ObservableCollection.Add(thisEvent);
                        }
                        else
                        {
                            //check if item no longer exists.
                            removedEvents++;
                        }
                    }
                    else
                    {
                        failedAttempts++;
                    }
                }
            }

            //remove spinner
            if (removedEvents > 0)
            {
                //Toast($"{removedEvents} were removed from your list because the no longer exist")
            }
            if (failedAttempts > 0)
            {
                //Toast($"There was a problem getting {failedAttempts} events")
            }
        }

        private async void EventListItem_OnClick(object sender, SelectedItemChangedEventArgs e)
        {
            var selectedEvent = (Event) e.SelectedItem;
            await Navigation.PushAsync(new EventPage(selectedEvent), true);
            
        }
    }
}