﻿using System;
using System.Net.Http;
using QuicEventMobile.Models;
using Xamarin.Forms;

namespace QuicEventMobile
{
	public partial class MainPage
	{
		public MainPage()
		{
            NavigationPage.SetHasNavigationBar(this, false);
			InitializeComponent();
        }

        private void ScheduleIdEntry_OnUnfocused(object sender, FocusEventArgs e)
	    {
	        ScheduleIdEntry.Text = ScheduleIdEntry.Text?.ToUpper().Trim();
	    }

	    private void SearchBtn_OnClicked(object sender, EventArgs e)
	    {
	        LoadSchedule(ScheduleIdEntry.Text);
	    }

        private async void LoadSchedule(string scheduleId)
	    {
	        var url = "http://test-api.quicevent.com/api/event";

#if !DEBUG
            url = "http://api.quicevent.com/api/event";
#endif

            using (var client = new HttpClient())
            {
                //client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.GetAsync($"{url}/{scheduleId}/withrelated");
                if (response.IsSuccessStatusCode)
                {
                    var apiResult = await response.Content.ReadAsAsync<ApiResult<Event>>();
                    if (null == apiResult.Meta)
                    {
                        await Navigation.PushAsync(new EventPage(apiResult.Data), true);
                    }
                }
                else
                {
                    MyTextArea.Text = "Nah G...";
                }
            }
        }
	}
}
