﻿using QuicEventMobile.Models;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QuicEventMobile
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventPage
    {
        private Event MyEvent { get; }

        public EventPage(Event myEvent)
        {
            MyEvent = myEvent;

            NavigationPage.SetHasNavigationBar(this, false);
            if (null != myEvent)
            {
                InitializeComponent();

                EventName.Text = MyEvent.Name;
                StartDate.Text = MyEvent.StartDateFormatted;
                StartTime.Text = MyEvent.StartTimeFormatted;
                ToggleSaveButtonState();

                MapPlaceholder.Source = ImageSource.FromUri(new Uri($"https://maps.googleapis.com/maps/api/staticmap?center={MyEvent.Location}&zoom=14&size=2048x2048&scale=2&maptype=roadmap&markers=color:0x6ccacd|label:Q|{MyEvent.Location}&key=AIzaSyByhebHqa9eYqtSpnHLoLWU2KfY_0L3Z9k")); 
            }
            else
            {
                Navigation.PopAsync(true);
            }
        }

        private void SaveRemoveBtn_OnClicked(object sender, EventArgs e)
        {
            var eventIdListAsString = EventHelper.GetEventListAsString();
            if (IsEventSaved())
            {
                var eventIdList = eventIdListAsString.Split('|');
                eventIdListAsString = string.Join("|", eventIdList.Where(ev => !ev.Equals(MyEvent.Id)));
                //Toast("Event removed from event list");
            }
            else
            {
                eventIdListAsString += $"{MyEvent.Id}|";
                //Toast("Event added to event list");
            }

            EventHelper.SaveEventList(eventIdListAsString);
            ToggleSaveButtonState();
        }

        public bool IsEventSaved()
        {
            var eventIdList = EventHelper.GetEventListAsArray();
            return eventIdList.Any(ev => ev.Equals(MyEvent.Id));
        }

        private void ToggleSaveButtonState()
        {
            if (IsEventSaved())
            {
                SaveRemoveBtn.Text = "Remove";
                SaveRemoveBtn.BackgroundColor = (Color)Application.Current.Resources["QuicRed"];
            }
            else
            {
                SaveRemoveBtn.Text = "Save";
                SaveRemoveBtn.BackgroundColor = (Color)Application.Current.Resources["QuicDefault"];
            }
        }

        private async void EnterBtn_OnClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new EventSchedulePage(MyEvent));
        }

        private async void CalendarImageTapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new EventListPage());
        }
    }
}