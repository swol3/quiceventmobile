﻿using System;
using Xamarin.Forms;

namespace QuicEventMobile
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public async void OnHomeButtonPressed(object sender, EventArgs e)
        {
            await MainPage.Navigation.PopToRootAsync();
        }

        public async void OnBackButtonPressed(object sender, EventArgs e)
        {
            await MainPage.Navigation.PopAsync();
        }

        private async void OnCalendarButtonPressed(object sender, EventArgs e)
        {
            await MainPage.Navigation.PushAsync(new EventListPage());
        }
    }
}
